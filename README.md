# Email signature

## How to use this template

1. Fill up with your data, use BASE 64 encoded images
2. Use CSS Inliner as e.g https://putsmail.com/inliner
3. Copy the inlined HTML to your Email Client Signature section

Created by joearpy - [joearpy87@gmail.com](mailto:joearpy87@gmail.com)
